<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-paged-iterator-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Iterator;

use Stringable;

/**
 * PagedDataProviderInterface interface file.
 * 
 * This data provider is a wrapper around a method from a class that gets data
 * according to unknown data for this object, and the number of the page that
 * is desired to be visited.
 * 
 * @author Anastaszor
 * @template T of object
 */
interface PagedDataProviderInterface extends Stringable
{
	
	/**
	 * Gets the data from the page number that is used.
	 * 
	 * @param integer $pagenb
	 * @return PagedIteratorInterface<T>
	 */
	public function provideData(int $pagenb = 1) : PagedIteratorInterface;
	
}
