<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-paged-iterator-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Iterator;

use Iterator;
use Stringable;

/**
 * PagedIteratorInterface interface file.
 * 
 * This file represents an iterator that carries information about the context
 * from which it is drawn with pagination data.
 * 
 * @author Anastaszor
 * @template T of object
 * @extends \Iterator<integer|string, T>
 */
interface PagedIteratorInterface extends Iterator, Stringable
{
	
	/**
	 * Gets the number of the first page that is available.
	 * 
	 * @return integer
	 */
	public function getFirstPage() : int;
	
	/**
	 * Gets the number of the previous page, or null if there are no previous
	 * page.
	 * 
	 * @return ?integer
	 */
	public function getPrevPage() : ?int;
	
	/**
	 * Gets the number of the current page. Paginations ought to be human
	 * readable, and begin at the number 1.
	 * 
	 * @return integer
	 */
	public function getCurrentPage() : int;
	
	/**
	 * Gets the number of the next page, or null if there are no next page.
	 * 
	 * @return ?integer
	 */
	public function getNextPage() : ?int;
	
	/**
	 * Gets the number of the last page that is available.
	 * 
	 * @return integer
	 */
	public function getLastPage() : int;
	
}
